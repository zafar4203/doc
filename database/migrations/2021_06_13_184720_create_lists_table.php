<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('relationship_type')->nullable();
            $table->string('email_id')->nullable();
            $table->string('designation')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('nationality')->nullable();
            $table->string('city')->nullable();
            $table->string('current_city')->nullable();
            $table->string('info')->nullable();
            $table->string('qualifier')->nullable();
            $table->string('vb')->nullable();
            $table->string('closed')->nullable();
            $table->string('reason')->nullable();
            $table->string('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lists');
    }
}
