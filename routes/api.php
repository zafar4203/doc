<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::post('authenticate', 'UserController@authenticate');

// My List Routes Start
Route::get('lists', 'MyListController@getLists');
Route::post('addList', 'MyListController@addList');
Route::post('updateList', 'MyListController@updateList');
Route::post('importList', 'MyListController@importList');
Route::get('list/{id}', 'MyListController@getSingleList');
// My List Routes End

// Appointment Routes Start
Route::get('appointments', 'AppointmentController@getAppointments');
Route::post('addAppointment', 'AppointmentController@addAppointment');
Route::post('updateAppointment', 'AppointmentController@updateAppointment');
Route::get('appointment/{id}', 'AppointmentController@getSingleAppointment');
// Appointment Routes End

Route::group(['middleware' => ['jwt.verify' , 'cors']], function () {

    Route::get('user', 'UserController@getAuthenticatedUser')->middleware('isAdmin');
    Route::post('userUpdate', 'UserController@userUpdate');
    Route::post('updatePassword', 'UserController@updatePassword');

});
