<!DOCTYPE html>
<html lang="en">
  <head>
    @include('partials.head')
  </head>
  <style>
    .VueTables{
      padding:0px 20px;
    }
    .VueTables .form-inline .VueTables__search-field label,
    .VueTables .form-inline .VueTables__limit-field label{
          display:none !important;
      }
      .VuePagination {
        margin-top:3%;
      }

      .select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 40px;
    user-select: none;
    width:100%;
    -webkit-user-select: none;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 40px;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 40px;
    position: absolute;
    top: 1px;
    right: 1px;
    width: 20px;
}
  </style>
  <body> 

<div id="app">
    <main-component></main-component>
</div>    
@include('partials.footer-scripts')

  </body>
</html>
