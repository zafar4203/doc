<script src="{{ asset('public/js/app.js') }}"></script>
<script src="public/assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="public/assets/js/popper.min.js"></script>
		<script src="public/assets/js/bootstrap.min.js"></script>
		<!-- Datetimepicker JS -->
		<script src="public/assets/js/moment.min.js"></script>
		<script src="public/assets/js/bootstrap-datetimepicker.min.js"></script>
		<script src="public/assets/plugins/daterangepicker/daterangepicker.js"></script>
		<!-- Full Calendar JS -->
        <script src="public/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="public/assets/plugins/fullcalendar/fullcalendar.min.js"></script>
        <script src="public/assets/plugins/fullcalendar/jquery.fullcalendar.js"></script>
		<!-- Sticky Sidebar JS -->
        <script src="public/assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="public/assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		<!-- Select2 JS -->
		<script src="public/assets/plugins/select2/js/select2.min.js"></script>
			<!-- Fancybox JS -->
			<script src="public/assets/plugins/fancybox/jquery.fancybox.min.js"></script>
		<!-- Dropzone JS -->
		<script src="public/assets/plugins/dropzone/dropzone.min.js"></script>
		
		<!-- Bootstrap Tagsinput JS -->
		<script src="public/assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
		
		<!-- Profile Settings JS -->
		<script src="public/assets/js/profile-settings.js"></script>
		<!-- Circle Progress JS -->
		<script src="public/assets/js/circle-progress.min.js"></script>
		<!-- Slick JS -->
		<script src="public/assets/js/slick.js"></script>
		
		<!-- Custom JS -->
		<script src="public/assets/js/script.js"></script>
		@if(Route::is(['map-grid','map-list']))
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6adZVdzTvBpE2yBRK8cDfsss8QXChK0I"></script>
		<script src="public/assets/js/map.js"></script>
		@endif
