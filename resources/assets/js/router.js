import VueRouter from 'vue-router'
import Vue from 'vue'
import Login from './components/Login.vue'
import Register from './components/Register.vue'
import Doctor from './components/Doctor/Doctor.vue'
import ProfileSettings from './components/Doctor/ProfileSettings.vue'
import MyList from './components/Doctor/MyList/MyList.vue'
import store from "./store";

Vue.use(VueRouter);

const routes = [
    {path:"/", component:Login},
    {path:"/login", component:Login},
    {path:"/register", component:Register},
    {
      path:"/doctor", 
      component:Doctor,
      children: [
      {
        path: 'profile-settings',
        component: ProfileSettings,
      },
      {
        path: 'my-list',
        component: MyList,
      }
    ]
    },

];
const router = new VueRouter({
    routes:routes,    
})

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
      if (store.getters.isAuthenticated) {
        next()
        return
      }
      next('/login')
    } else {
      next()
    }
  });

  router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.guest)) {
      if (store.getters.isAuthenticated) {
        next("/posts");
        return;
      }
      next();
    } else {
      next();
    }
  });

  
export default router