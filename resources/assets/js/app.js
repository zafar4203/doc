const { default: router } = require('./router');

require('./bootstrap');

window.Vue = require('vue');
import Router from './router.js'
import store from './store';
import Vuelidate from 'vuelidate';
import axios from 'axios';
import {ServerTable} from 'vue-tables-2';


axios.defaults.withCredentials = true
// axios.interceptors.response.use(res => res , error => {
//     if (error) {
//       const originalRequest = error.config;
//       if (error.response.status === 401 && !originalRequest._retry) {
    
//           originalRequest._retry = true;
//         //   store.dispatch('LogOut')
//           return router.push('/login')
//       }
//       throw error;
//     }
// });

axios.interceptors.request.use(function (config) {
    let token = store.getters.token;
    config.headers.Authorization =  'Bearer '+ token;
    return config;
});

Vue.use(Vuelidate);
Vue.use(ServerTable, {}, false, 'bootstrap4' , 'default');
Vue.component('main-component', require('./components/Main.vue'));
Vue.component('header-component', require('./components/Header.vue'));
Vue.component('footer-component', require('./components/Footer.vue'));

Vue.config.productionTip = false
const app = new Vue({
    el: '#app',
    store,
    router:Router,
});

