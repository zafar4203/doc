import Vuex from 'vuex';
import Vue from 'vue';
import createPersistedState from "vuex-persistedstate";
import auth from './modules/auth';
import list from './modules/list';
import appointment from './modules/appointment';

// Load Vuex
Vue.use(Vuex);
// Create store
export default new Vuex.Store({
  modules: {
    auth,
    list,
    appointment
  },
  plugins: [createPersistedState()]
});