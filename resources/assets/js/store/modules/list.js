import axios from "axios";
import { createNamespacedHelpers } from "vuex";

const state = {
  lists: null,
};

const getters = {

};

const actions = {
  async CreateList({ dispatch }, post) {
    await axios.post("http://localhost/doc/api/addList", post);
    return await dispatch("GetLists");
  },

  async UpdateList({ dispatch }, post) {
    await axios.post("http://localhost/doc/api/updateList", post);
    return await dispatch("GetLists");
  },

  async GetLists({ commit }) {
    let response = await axios.get("http://localhost/doc/api/lists");
    commit("setLists", response.data);
  },

  async GetSingleList({} , id) {
    const resp = await axios.get("http://localhost/doc/api/list/"+id);
    return resp.data;
  },

  async ImportList({}, formData) {
    let resp = await axios.post('http://localhost/doc/api/importList', formData , {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
    return resp;
  },

};

const mutations = {
  setLists(state, posts) {
    state.posts = posts;
  }
};

export default {
  state,
  getters,
  actions,
  mutations,
};