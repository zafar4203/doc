import axios from "axios";

const state = {
  user: null,
  posts: null,
};

const getters = {
  isAuthenticated: (state) => !!state.user,
  StatePosts: (state) => state.posts,
  user: (state) => state.user,
  token: (state) => state.token,
};

const actions = {
  async Register({dispatch}, form) {
    await axios.post('http://localhost/doc/api/register', form)
    let UserForm = new FormData()
    UserForm.append('email', form.email)
    UserForm.append('password', form.password)
    await dispatch('LogIn', UserForm)
  },

  async LogIn({commit}, user) {
    let resp = await axios.post("http://localhost/doc/api/login", user);
    await commit("setUser", resp.data.user);
    await commit("token", resp.data.token);
  },

  async updateUser({commit}, formData) {
    let resp = await axios.post('http://localhost/doc/api/userUpdate', formData , {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
    await commit("setUser", resp.data.user);
  },

  async updatePassword({commit}, user) {  
    let resp = await axios.post('http://localhost/doc/api/updatePassword', user);
    await commit("setUser", resp.data.user);
  },


  async LogOut({ commit }) {
    let user = null;
    commit("logout", user);
  },
};

const mutations = {
  setUser(state, user) {
    state.user = user;
  },
  token(state, token) {
    state.token = token;
  },
  logout(state, user) {
    state.user = user;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};