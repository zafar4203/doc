import axios from "axios";
import { createNamespacedHelpers } from "vuex";

const state = {
  appointments: null,
};

const getters = {

};

const actions = {
  async CreateAppointment({ dispatch }, post) {
    await axios.post("http://localhost/doc/api/addAppointment", post);
    return await dispatch("GetAppointments");
  },

  async UpdateAppointment({ dispatch }, post) {
    await axios.post("http://localhost/doc/api/updateAppointment", post);
    return await dispatch("GetAppointments");
  },

  async GetAppointments({ commit }) {
    let response = await axios.get("http://localhost/doc/api/appointments");
    commit("setAppointments", response.data);
  },

  async GetSingleAppointment({} , id) {
    const resp = await axios.get("http://localhost/doc/api/appointment/"+id);
    return resp.data;
  },

};

const mutations = {
  setAppointments(state, appointments) {
    state.appointments = appointments;
  }
};

export default {
  state,
  getters,
  actions,
  mutations,
};