<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'gender' => 'required',
            'cnic' => 'required|numeric',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'cnic' => $request->get('cnic'),
            'referal' => $request->get('referal'),
            'gender' => $request->get('gender'),
            'password' => Hash::make($request->get('password')),
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'),201);
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $userdata = array(
            'email'     => $request->email,
            'password'  => $request->password
        );
    
        // attempt to do the login
        if (Auth::attempt($userdata)) {
    
            $user = Auth::user();
            $token = JWTAuth::fromUser($user);
            return response()->json(compact('user','token'),201);
        
        } else {            
            $validator = Validator::make([], []); // Empty data and rules fields
            $validator->errors()->add('email', 'Invalid Login Details');

            return response()->json($validator->errors(), 400);
        }

    }

    public function getAuthenticatedUser()
        {
            try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
            }

            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                    return response()->json(['token_expired'], $e->getStatusCode());

            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                    return response()->json(['token_invalid'], $e->getStatusCode());

            } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                    return response()->json(['token_absent'], $e->getStatusCode());

            }

            return response()->json(compact('user'));
        }
        public function userUpdate(Request $request){
            try {
                if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
                }
            }catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                    return response()->json(['token_expired'], $e->getStatusCode());
            }catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                    return response()->json(['token_invalid'], $e->getStatusCode());
            }catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['token_absent'], $e->getStatusCode());
            }

            $request->user = json_decode($request->user , true);
            $validator = Validator::make($request->user, [
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'gender' => 'required',
                'cnic' => 'required|numeric',
                'email' => 'required|string|email|max:255|unique:users,id,' . $user->id,
            ]);
    
            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }
    
            if($request->image){
                $file = $request->image;
                $path1 = 'public/assets/images/users';
                $fileName1 = $user->id . rand() . ".png";
                $success1 = $file->move($path1, $fileName1);
                $request->user['image'] = asset('public/assets/images/users/'.$fileName1);
            }
            $user->fill($request->user);
            $user->save(); // no validation implemented
        
            return response()->json(compact('user'),201);
    
        }

     public function updatePassword(Request $request){
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        }catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                return response()->json(['token_expired'], $e->getStatusCode());
        }catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                return response()->json(['token_invalid'], $e->getStatusCode());
        }catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_absent'], $e->getStatusCode());
        }

        $validator = Validator::make($request->all(), [
            'c_password' => 'required|string|min:6',
            'n_password' => 'required|string|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        if(Hash::check($request->c_password , $user->password)){
            $user->password = Hash::make($request->n_password);
            $user->save();    

            return response()->json(compact('user'));

        }else{
            $validator = Validator::make([], []); // Empty data and rules fields
            $validator->errors()->add('c_password', 'Password not matched in records');

            return response()->json($validator->errors(), 400);
        }
    
     }   
}
