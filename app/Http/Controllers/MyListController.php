<?php

namespace App\Http\Controllers;

use App\Models\MyList;
use App\Models\ListsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class MyListController extends Controller
{
    public function getLists(Request $request)
    {
        if($request['query'])
        {
            $lists = MyList::select('*')
            ->where('first_name', 'like', '%' . $request['query'] . '%')
            ->orWhere('last_name', 'like', '%' . $request['query'] . '%')
            ->paginate($request->limit ? $request->limit : 20);
        }
        elseif($request->orderBy)
        {
            $lists = MyList::select('*')->orderBy($request->orderBy, $request->ascending == 1 ? 'ASC' : 'DESC')->paginate($request->limit ? $request->limit : 20);
        }
        else
        {
            $lists = MyList::select('*')->paginate($request->limit ? $request->limit : 20);
        }
        
        foreach($lists as $key => $list){
            $list->sr_no = $key + 1;
        }

        return response()->json($lists);
    }

    public function addList(Request $request){
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'relationship_type' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $myList = new MyList();
        $myList->fill($request->all());
        $myList->save();

        return response()->json(compact('myList'),201);
    }

    public function getSingleList($id){
        if($id){
            $myList = MyList::find($id);
            return response()->json(compact('myList'),201);
        }
    }

    public function updateList(Request $request){
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'relationship_type' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $myList = Mylist::find($request->id);
        $myList->fill($request->all());
        $myList->save();

        return response()->json(compact('myList'),201);
    }

    public function importList(Request $request){
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:xls,xlsx',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
      
        Excel::import(new ListsImport,request()->file('file'));
           
        return response()->json('Excel Data Imported successfully.',201);

        }
}
