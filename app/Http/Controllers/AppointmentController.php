<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appointment;
use Illuminate\Support\Facades\Validator;

class AppointmentController extends Controller
{
    public function getAppointments(Request $request)
    {
        if($request['query'])
        {
            $lists = Appointment::select('*')
            ->where('presenter', 'like', '%' . $request['query'] . '%')
            ->paginate($request->limit ? $request->limit : 20);
        }
        elseif($request->orderBy)
        {
            $lists = Appointment::select('*')->orderBy($request->orderBy, $request->ascending == 1 ? 'ASC' : 'DESC')->paginate($request->limit ? $request->limit : 20);
        }
        else
        {
            $lists = Appointment::select('*')->paginate($request->limit ? $request->limit : 20);
        }
        
        return response()->json($lists);
    }

    public function addAppointment(Request $request){
        $validator = Validator::make($request->all(), [
            'time' => 'required|string|max:255',
            'date' => 'required|string|max:255',
            'presenter' => 'required',
            'user_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $appointment = new Appointment();
        $appointment->fill($request->all());
        $appointment->save();

        return response()->json(compact('appointment'),201);
    }

    public function getSingleAppointment($id){
        if($id){
            $appointment = Appointment::find($id);
            return response()->json(compact('appointment'),201);
        }
    }

    public function updateList(Request $request){
        $validator = Validator::make($request->all(), [
            'time' => 'required|string|max:255',
            'date' => 'required|string|max:255',
            'presenter' => 'required',
            'user_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $appointment = Appointment::find($request->id);
        $appointment->fill($request->all());
        $appointment->save();

        return response()->json(compact('appointment'),201);
    }

}
