<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MyList extends Model 
{
    protected $fillable = [
        'first_name',
        'last_name',
        'relationship_type',
        'phone_no',
        'email_id',
        'designation',
        'nationality',
        'city',
        'current_city',
        'info',
        'qualifier',
        'vb',
        'reason',
        'closed'
    ];

}
