<?php
  
namespace App\Models;
  
use App\Models\MyList;
use Maatwebsite\Excel\Concerns\ToModel;
  
class ListsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new MyList([
            'first_name'     => $row[0],
            'last_name'      => $row[1], 
            'relationship_type' => $row[2],
        ]);
    }
}