<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = [

        'time',
        'date',
        'user_id',
        'presenter',
    ];
}
